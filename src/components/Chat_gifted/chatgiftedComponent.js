import React from 'react'
import { GiftedChat } from 'react-native-gifted-chat'
import SocketUtils from '../Socket/SocketUtils';

let socket = null;

class ChatGiftedScreen extends React.Component {
  state = {
    messages: [],

  }

  constructor(props) {
    super(props);
    this.state = {
      messages: null,
      user: null,
      sendTo: null
    };
  }



  componentDidMount(){
    console.log("SocketUtilsExConv:", this.state.user);
    console.log("sendTo:", this.state.sendTo);
  }

  componentWillMount() {

    const { navigation } = this.props;
    const data = navigation.getParam('messages');
    const data2= navigation.getParam('user');
    const data3= navigation.getParam('sendTo');
    socket = navigation.getParam('socket');
    socket.on('message', this.onReceivedMessage);
    console.log("messages: " + data + " user: " + data2 + " sendTo: " + data3 + " socket: " + socket)
    this.setState({ messages: data,  user:data2, sendTo: data3});

    /*this.setState({
      messages: [
        {
          _id: 1,
          text: 'Hello developer',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
        },
      ],
    })*/
  }

  onReceivedMessage = (messages) => {
    console.log('onReceivedMessage:', messages);
    console.log('messages[0].user.name:', messages[0].user.name);
    if(messages[0].user.name == this.state.sendTo){
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, messages),
      }))
    }

    //return messages;
  };

  onSend(messages = []) {
    console.log("onsend messages:", messages);
    socket.emit('message', messages);
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }))
    console.log('gifted msg:', this.state.messages);
  }

  render() {
    //const { navigation } = this.props;
    //const data = navigation.getParam('otherParam');
    //console.log("SocketUtilsExConv:", data);
    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={messages => this.onSend(messages)}
        user={{
          _id: this.state.user,
          name: this.state.user,
          sendTo: this.state.sendTo,
        }}
      />
    )
  }
}

export default ChatGiftedScreen;
