import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, Button, TouchableHighlight } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import SocketUtils from '../Socket/SocketUtils';

let USER = '';
let socket = null;
//let userListing = null;

export default class ListUserScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userListing: null,
      sendTo: null
    };
  }

  componentWillMount(){
    socket = SocketUtils.connect();
    //SocketUtils.handleOnMessage();
    //socket.on('message', this.onReceivedMessage);
    socket.on('existingConversation', this.onExistingConversationMessage);
    socket.on('userList', this.onReceivedUserList);
    socket.on('userJoined', this.onReceivedMessage);
    socket.on('error_msg', this.onReceivedMessage);
  }

  onExistingConversationMessage = (messages) => {
    console.log('onExistingConversationMessage:', messages);
    this.props.navigation.navigate('ChatGifted',{messages: messages, user: USER, sendTo: this.state.sendTo, socket: socket });
    //return messages;
  };

  onReceivedUserList = (data) => {
    console.log('onReceivedUserList:', data);
    this.setState({ userListing: data });
    //return messages;
  };

  componentDidMount() {
    this.getUserListing();
    //this.determineUser();
    //this._onPressButton();
    //SocketUtils.handleOnMessage();
    /*SocketUtils.handleOnMessage().then(data => {
      console.log("return handleOnMessage:", data);
    });*/
  }

  componentWillUnmount(){
    console.log("socket close");
    socket.close();
  }

  getUserListing = async () => {
    USER = await AsyncStorage.getItem('userId');
    if(USER){
      SocketUtils.emitUserJoined(USER);
    }
  }

  /*determineUser = async () => {
    USER = await AsyncStorage.getItem('userId');
    console.log("USER:", USER);
    this.setState({ userId: USER });
    this.setState({ sendTo: 'android' });
    let sendToId = this.state.sendTo;
    console.log("sendToId:", sendToId);
    //socket.emit('userJoined', 'android');
    //socket.emit('userJoined', USER);

    //socket.emit('retrieve_conversation', {USER, sendToId});
    if(USER && sendToId){
      SocketUtils.emitUserJoined(USER, sendToId);
      console.log('retrieve_conversation', {USER, sendToId});
    }
  }*/

  _onPressButton(){
    socket = SocketUtils.getSocket();
    console.log("_onPressButton socket", socket);
    socket.on('message', this.onReceivedMessage);
  }

  _onPress(data){
    console.log("_onPress", data);
    SocketUtils.emitExistingConversation(USER, data.userEmail);
    this.setState({sendTo: data.userEmail});

  }

  onReceivedMessage = (messages) => {
    console.log('onReceivedMessage:', messages);
    //return messages;
  };

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.userListing}
          renderItem={({item}) => (
            <TouchableHighlight onPress={() => this._onPress(item)}>
              <Text style={styles.item}>{item.userEmail}</Text>
            </TouchableHighlight>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
})

//backup
/*
<Button
  onPress={this._onPressButton}
  title="Press Me"
/>*/
