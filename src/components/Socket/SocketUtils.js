import SocketIOClient from 'socket.io-client';
import AsyncStorage from '@react-native-community/async-storage';

let USER = '';

let socket = null;
let existingConversation = null;



const connect = () => {
  console.log("running connect");
  socket = SocketIOClient('http://192.168.0.116:3000', {
    transports: ['websocket'] // you need to explicitly tell it to use websockets
  });
  return socket;
};

const getExistingConversation = () => {
  return existingConversation;
};

const getSocket = () => {
  return socket;
};

/*const handleOnMessage = () => {
  socket.on('message', this.onReceivedMessage);

};*/

onReceivedMessage = (messages) => {
  existingConversation = messages;
  console.log('onReceivedMessage in SUtil:', messages);
    //return messages;
};

onReceivedMessageUserJoined = (messages) => {
    console.log('onReceivedMessageUserJoined:', messages);
  };

const emitUserJoined = (USER) => {
  //socket.emit('retrieve_conversation', {USER, sendToId});
  socket.emit('userJoined', USER);
};

const emitExistingConversation = (USER, sendToId) => {
  console.log("emitUserJoined socket", socket);
  socket.emit('retrieve_conversation', {USER, sendToId});
  //socket.emit('getExistingConversation', USER);
  console.log('retrieve_conversation', {USER, sendToId});
};

const SocketUtils = {
  connect,
  //handleOnMessage,
  emitUserJoined,
  emitExistingConversation,
  getSocket,
  getExistingConversation
};
export default SocketUtils;
