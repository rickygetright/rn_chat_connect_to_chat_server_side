/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

 import React from 'react';
import SocketIO from 'socket.io-client'
import WebSocketClient from 'reconnecting-websocket'
import {RTCPeerConnection,
RTCIceCandidate,
RTCSessionDescription,
RTCView,
MediaStream,
MediaStreamTrack,
mediaDevices} from 'react-native-webrtc'
import UUID from 'react-native-device-uuid'

import { StyleSheet, Text, View, TouchableOpacity, Dimensions, FlatList } from 'react-native';

const dimensions = Dimensions.get('window')

const HOST = process.env.HOST || 'wss://192.168.0.115:6503'
const isFront = true // Use Front camera?
const DEFAULT_ICE = {
// we need to fork react-native-webrtc for relay-only to work.
//  iceTransportPolicy: "relay",
  iceServers: [
    // {
    //   urls: 'turn:s2.xirsys.com:80?transport=tcp',
    //   username: '8a63bcac-e16a-11e7-a86e-a62bc0457e71',
    //   credential: '8a63bdd8-e16a-11e7-b7e2-48f12b7ac2d8'
    // },
    {
      //urls: 'turn:turn.msgsafe.io:443?transport=tcp',
      urls: 'turn:192.168.0.115',
      username: 'webrtc',
      credential: 'turnserver'
    },
    // Native libraries DO NOT fail over correctly.
    //{
    //  urls: 'turn:turn.msgsafe.io:443',
    //  username: 'a9a2b514',
    //  credential: '00163e7826d6'
    //}

  ]
}

export default class App extends React.Component {

  constructor(props) {
    super(props)
    this.handleConnect = this.handleConnect.bind(this)
    this.on_ICE_Connection_State_Change = this.on_ICE_Connection_State_Change.bind(this)
    this.on_Add_Stream = this.on_Add_Stream.bind(this)
    this.on_ICE_Candiate = this.on_ICE_Candiate.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
    this.on_Offer_Received = this.on_Offer_Received.bind(this)
    this.on_Answer_Received = this.on_Answer_Received.bind(this)
    this.setupWebRTC = this.setupWebRTC.bind(this)
    this.handleAnswer = this.handleAnswer.bind(this)
    this.on_Remote_ICE_Candidate = this.on_Remote_ICE_Candidate.bind(this)

    this.state = {
      connected: false,
      ice_connection_state: '',
      pendingCandidates: [],
      userList: []
    }
  }


  componentWillUnmount() {
    if (this.peer) {
      this.peer.close()
    }
  }
  componentDidMount() {
    this.socketconnection();
  }

  socketconnection(){
    const options = {
    //WebSocket: WS, // custom WebSocket constructor
    debug: true,
    maxReconnectionDelay: 10000,
    minReconnectionDelay: 1000 + Math.random() * 4000,
    reconnectionDelayGrowFactor: 1.3,
    connectionTimeout: 100,
    maxRetries: 10,
    };

    // Setup Socket
    const ws = new WebSocketClient('ws://192.168.0.115:6503/','json', options);
    const self = this
    self.ws = ws

    mediaDevices.enumerateDevices().then(sourceInfos => {
      console.log('sourceInfos: ', sourceInfos);
      let videoSourceId;
      for (let i = 0; i < sourceInfos.length; i++) {
        const sourceInfo = sourceInfos[i];
        if(sourceInfo.kind == "videoinput" && sourceInfo.facing == (isFront ? "front" : "back")) {
          videoSourceId = sourceInfo.deviceId;
        }
      }
      mediaDevices.getUserMedia({
        audio: true,
        video: {
          mandatory: {
            minWidth: 500, // Provide your own width, height and frame rate here
            minHeight: 300,
            minFrameRate: 30
          },
          facingMode: (isFront ? "user" : "environment"),
          optional: (videoSourceId ? [{sourceId: videoSourceId}] : [])
        }
      })
      .then(stream => {
        self.setState({
          localStreamURL: stream.toURL()
        })
        self.localStream = stream
        //this.localStream = stream
      })
      .catch(error => {
        console.error('Failed to setup stream:', e.message)
      });
    });

    ws.onopen = () => {
      console.info('Socket Connected!')
      self.setState({
        connected: true
      })
    };

    ws.onmessage = e => {
      self.setState({
        connected: true
      })
      let msg = {}
      const { data } = e
      try { msg = JSON.parse(data)} catch(e) {
        console.error('Invalid message:', data)
      }
      console.info('New Message:', data)
      // a message was received
      if (msg) {
        if (msg.type === 'video-offer') {
          this.on_Offer_Received(msg)
        } else if (msg.type === 'answer') {
          this.on_Answer_Received(msg)
        } else if(msg.type === 'id'){
          var date = new Date().getTime();
          console.log('date now:', date);
          this.sendMessage({
            type: 'username',
            name: 'from_meizu',
            date: date,
            id: msg.id
          })
        } else if(msg.type === 'userlist'){
          this.setState({
            userList: msg.users
          })
          console.log('new userList: ', this.state.userList)
        } else if(msg.type === 'new-ice-candidate'){
          //this.handleAnswer(msg)
          //this.on_Answer_Received(msg);
          this.on_Remote_ICE_Candidate(msg);
        } else {
          console.error('Unknown message:', msg)
        }
      }
    };

    ws.onerror = e => {
      // an error occurred
      console.info(e.message);
      self.setState({
        connected: false
      })
    };

    ws.onclose = e => {
      // connection closed
      console.log(e.code, e.reason);
      self.setState({
        connected: false
      })
    };

    // Setup Camera & Audio
    //mediaDevices.enumerateDevices().then(sourceInfos => {
    //    let videoSourceId;
    //    for (let i = 0; i < sourceInfos.length; i++) {
    //      const sourceInfo = sourceInfos[i];
    //      if(sourceInfo.kind == "video" && sourceInfo.facing == (isFront ? "front" : "back")) {
    //        videoSourceId = sourceInfo.id;
    //      }
    //    }
    //    return mediaDevices({
    //      audio: true,
    //      video: {
    //        mandatory: {
    //          minWidth: 500, // Provide your own width, height and frame rate here
    //          minHeight: 300,
    //          minFrameRate: 30
    //        },
    //        facingMode: (isFront ? "user" : "environment"),
    //        optional: (videoSourceId ? [{sourceId: videoSourceId}] : [])
    //      }
    //    });
    //  })
    //  .then(stream => {
    //    self.setState({
    //      localStreamURL: stream.toURL()
    //    })
    //    self.localStream = stream
    //  })
    //  .catch(e => {
    //    console.error('Failed to setup stream:', e.message)
    //  })

  }



  async setupWebRTC() {
    const self = this
    const peer = new RTCPeerConnection(DEFAULT_ICE)
    peer.oniceconnectionstatechange = this.on_ICE_Connection_State_Change
    peer.onaddstream = this.on_Add_Stream
    peer.onicecandidate = this.on_ICE_Candiate

    console.info('localStream:', this.localStream)
    peer.addStream(this.localStream)
    this.peer = peer
  }

  async handleConnect(e) {
    await this.setupWebRTC()
    const { peer } = this

    try {
            // Create Offer
      const offer = await peer.createOffer()
      console.info('Offer Created:', offer)
      self.offer = offer
      console.info('offer:', offer)

      await peer.setLocalDescription(offer)
      console.info('localDescription set!')

      // TODO: should send localDescription or offer
      // For now send localDescription


    } catch (e) {
      console.error('Failed to setup local offer')
      console.error(e.message)
      return
    }
  }

  on_ICE_Connection_State_Change(e) {
    console.info('ICE Connection State Changed:', e.target.iceConnectionState)
    this.setState({
      ice_connection_state: e.target.iceConnectionState
    })

    switch (e.target.iceConnectionState) {
      case 'closed':
      case 'disconnected':
      case 'failed':
        if (this.peer) {
          this.peer.close()
          this.setState({
            remoteStreamURL: null
          })
          this.remoteStream = null
        }
        break
    }
  }

  on_ICE_Candiate(e) {
    console.log('running on_ICE_Candiate');
    const { candidate } = e
    console.info('ICE Candidate Found:', candidate)

    if (candidate) {
      let pendingRemoteIceCandidates = this.state.pendingCandidates
      if (Array.isArray(pendingRemoteIceCandidates)) {
        this.setState({
          pendingCandidates: [...pendingRemoteIceCandidates, candidate]
        })
      } else {
        this.setState({
          pendingCandidates: [candidate]
        })
      }
    } else { // Candidate gathering complete
      if (this.state.pendingCandidates.length > 1) {
        this.sendMessage({
          type: this.state.offer_received ? 'answer' : 'offer',
          payload: {
            description: this.peer.localDescription,
            candidates: this.state.pendingCandidates
          }
        })
      } else {
        console.error('Failed to send an offer/answer: No candidates')
        debugger
      }
    }
  }

  async on_Remote_ICE_Candidate(data) {
    //console.log('this.peer: ', this.peer);
    if(!this.peer){
      await this.setupWebRTC();
      console.log('this.peer2: ', this.peer);
    }
    console.log('data: ', data.candidate);
    if (data.candidate) {
      if (this.peer) {
        //console.log('candidate: ', data.candidate);
        let parsed_candidate = JSON.stringify(data.candidate);
        //let parsed_candidate = JSON.parse(data.candidate);
        console.log('parsed_candidate: ', parsed_candidate);
        //console.log('candidate: ', data.candidate.candidate);
        //console.log('sdpMid: ', data.candidate.sdpMid);
        //console.log('sdpMLineIndex: ', data.candidate);
        //new RTCIceCandidate(parsed_candidate)
        //{data.candidate.candidate, data.candidate.sdpMid, data.candidate.sdpMLineIndex}
        //await this.peer.addIceCandidate(new RTCIceCandidate(data.candidate)).then(() => {
        //{candidate: data.candidate.candidate, sdpMid: new_sdpmid, sdpMLineIndex: data.candidate.sdpMLineIndex}
        let new_sdpmid = null;
        if(data.candidate.sdpMid == 0){
          new_sdpmid = 'audio';
        }else if(data.candidate.sdpMid == 1){
          new_sdpmid = 'video';
        }else{
          new_sdpmid = null;
        }

        let new_parsed_candidate = JSON.stringify({"candidate": null,"sdpMid": null, "sdpMLineIndex":data.candidate.sdpMLineIndex});
        //new_parsed_candidate = JSON.parse(new_parsed_candidate);
        console.log('new_parsed_candidate: ', new_parsed_candidate);

        console.log('new_sdpmid: ', new_sdpmid);
        await this.peer.addIceCandidate(new RTCIceCandidate(new_parsed_candidate)).then(() => {
              console.log('WebRTC: Successfully added iceCandidate to client: iceConnectionState:');
            }).catch((error) => {
              console.warn('WebRTC: Error adding iceCandidate:', error);
            });
      } else {
        console.error('Peer is not ready')
      }
    } else {
      console.info('Remote ICE Candidates Gathered!')
    }
  }

  on_Add_Stream(e) {
    console.info('Remote Stream Added:', e.stream)
    this.setState({
      remoteStreamURL: e.stream.toURL()
    })
    this.remoteStream = e.stream
  }

  on_Offer_Received(data) {
    //debugger
    this.setState({
      offer_received: true,
      offer_answered: false,
      offer: data
    })
  }

  /*async on_Answer_Received(data) {
    const { payload } = data
    await this.peer.setRemoteDescription(new RTCSessionDescription(payload.description))
    payload.candidates.forEach(c => this.peer.addIceCandidate(new RTCIceCandidate(c)))
    this.setState({
      answer_recevied: true
    })
  }*/
  async on_Answer_Received(data) {
    const { offer_data } = this.state.offer
    const { payload } = data
    await this.peer.setRemoteDescription(new RTCSessionDescription(offer_data.sdp))
    payload.candidates.forEach(c => this.peer.addIceCandidate(new RTCIceCandidate(c)))
    this.setState({
      answer_recevied: true
    })
  }

  async handleNewICECandidateMsg(data) {
    const { offer_data } = this.state.offer
    const { payload } = data
    //payload.candidates.forEach(c => this.peer.addIceCandidate(new RTCIceCandidate(c)))
  }

  async handleAnswer(msg) {
    const { payload } = this.state.offer
    await this.setupWebRTC()

    const { peer } = this

    await peer.setRemoteDescription(new RTCSessionDescription(payload.sdp))

    if (Array.isArray(payload.candidates)) {
      payload.candidates.forEach((c) => peer.addIceCandidate(new RTCIceCandidate(c)))
    }
    const answer = await peer.createAnswer()
    await peer.setLocalDescription(answer)
    this.setState({
      offer_answered: true
    })
  }

  sendMessage(msgObj) {
    const { ws } = this
    if (ws && ws.readyState === 1) {
      ws.send(JSON.stringify(msgObj))
    } else {
      const e = {
        code: 'websocket_error',
        message: 'WebSocket state:' + ws.readyState
      }
      throw e
    }
  }




  render() {
    return (
      <View style={styles.container}>

        <View style={styles.video}>

          <View style={styles.callerVideo}>
            <View style={styles.videoWidget}>
              { this.state.localStreamURL &&
                <RTCView streamURL={this.state.localStreamURL} style={styles.rtcView}/>
              }
            </View>
          </View>
          <View style={styles.calleeVideo}>
            <View style={styles.videoWidget}>
              { this.state.remoteStreamURL &&
                <RTCView streamURL={this.state.remoteStreamURL} style={styles.rtcView}/>
              }
            </View>
          </View>
        </View>
        <FlatList
          data={this.state.userList}
          renderItem={({item}) => <Text>{item}</Text>}
          keyExtractor={(item, index) => index.toString()}
        />
        <View style={ this.state.connected ? styles.onlineCircle : styles.offlineCircle}/>
        <View style={styles.bottomView}>
          <TouchableOpacity onPress={this.handleConnect} disabled={this.state.offer_received}>
            <Text style={styles.connect}>
              Connect
            </Text>
          </TouchableOpacity>
          { // Offer received and offer not answered
            (this.state.offer_received && !this.state.offer_answered) &&
            <TouchableOpacity onPress={this.handleAnswer}>
              <Text style={styles.connect}>
                Answer
              </Text>
            </TouchableOpacity>
          }
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 10,
    color: '#f00'
  },
  bottomView: {
    height: 20,
    flex: 1,
    bottom: 80,
    position: 'absolute',
    alignItems: 'center'
  },
  connect: {
    fontSize: 30
  },
  video: {
    flex: 1,
    flexDirection: 'row',
    position: 'relative',
    backgroundColor: '#eee',
    alignSelf: 'stretch'
  },
  onlineCircle: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: '#1e1',
    position: 'absolute',
    top: 10,
    left: 10
  },
  offlineCircle: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: '#333'
  },
  callerVideo: {
    flex: 0.5,
    backgroundColor: '#faa',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  calleeVideo: {
    flex: 0.5,
    backgroundColor: '#aaf',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  videoWidget: {
    position: 'relative',
    flex: 0.5,
    backgroundColor: '#fff',
    width: dimensions.width / 2,
    borderWidth: 1,
    borderColor: '#eee'
  },
  rtcView: {
    flex: 1,
    width: dimensions.width / 2,
    backgroundColor: '#f00',
    position: 'relative'
  }
});

/*import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput} from 'react-native';
import {
  RTCPeerConnection,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStream,
  MediaStreamTrack,
  mediaDevices
} from 'react-native-webrtc';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      videoURL: null,
      text: ''

    }
  }

  componentDidMount(){
    const configuration = {"iceServers": [{
        urls: "turn:192.168.0.115:6503",  // A TURN server
        username: "webrtc",
        credential: "turnserver"
      }]};
    const pc = new RTCPeerConnection(configuration);

    let isFront = true;
    mediaDevices.enumerateDevices().then(sourceInfos => {
      console.log(sourceInfos);
      let videoSourceId;
      for (let i = 0; i < sourceInfos.length; i++) {
        const sourceInfo = sourceInfos[i];
        if(sourceInfo.kind == "videoinput" && sourceInfo.facing == (isFront ? "front" : "back")) {
          videoSourceId = sourceInfo.deviceId;
        }
      }
      mediaDevices.getUserMedia({
        audio: true,
        video: {
          mandatory: {
            minWidth: 500, // Provide your own width, height and frame rate here
            minHeight: 300,
            minFrameRate: 30
          },
          facingMode: (isFront ? "user" : "environment"),
          optional: (videoSourceId ? [{sourceId: videoSourceId}] : [])
        }
      })
      .then(stream => {
        // Got stream!
        console.log("stream", stream.toURL());
        this.setState({
          videoURL: stream.toURL()
        });
      })
      .catch(error => {
        console.log("error", error);
        // Log error
      });
    });

    pc.createOffer().then(desc => {
      pc.setLocalDescription(desc).then((data) => {
        console.log('data', data);
        // Send pc.localDescription to peer
      });
    });

    pc.onicecandidate = function (event) {
      // send event.candidate to peer
    };

    // also support setRemoteDescription, createAnswer, addIceCandidate, onnegotiationneeded, oniceconnectionstatechange, onsignalingstatechange, onaddstream

  }

  render() {
    return (
      <View style={styles.container}>
        <Text>{this.state.videoURL}</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => this.setState({text})}
          value={this.state.videoURL}
        />
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={(text) => this.setState({text})}
          value={this.state.text}
        />
        <RTCView streamURL={this.state.text} style={styles.streamContainer}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    backgroundColor: '#F5FCFF',

  },
  streamContainer: {
    flex: 1,
    backgroundColor: '#ccc',
    borderWidth: 1,
    borderColor: '#000'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});*/

/* backup
<View style={styles.container}>
<Text>Here:</Text>
  <RTCView streamURL={this.state.videoURL}/>
</View>
*/
