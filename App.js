import React from 'react';
//import { Ionicons } from '@expo/vector-icons';
import { Ionicons } from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
//import { createStackNavigator } from 'react-navigation-stack';
//import { createBottomTabNavigator } from 'react-navigation-tabs';

import DetailsScreen from './src/components/About/AboutComponent';
import MainComponent from './src/components/MainScreen/MainComponent';
import LoginScreen from './src/components/LoginScreen/LoginComponent';
import ChatGiftedScreen from './src/components/Chat_gifted/chatgiftedComponent';
import ListUserScreen from './src/components/ChatScreen/ListUserComponent';

const MainStack = createStackNavigator({
  MainComponent: MainComponent,
  Details: DetailsScreen,
});

const ChatStack = createStackNavigator({
  Login: LoginScreen,
  ListUser: ListUserScreen,
  ChatGifted: ChatGiftedScreen,
});

/*const ChatGiftedStack = createStackNavigator({
  ChatGifted: ChatGiftedScreen,
});*/

export default createBottomTabNavigator(
  {
    MainScreen: MainStack,
    Chat: ChatStack,
    //ChatGiftedd: ChatGiftedStack
  },
  {
    navigationOptions: ({ navigation }) => ({
      /*tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;

        if (routeName === 'MainScreen') {
          iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        } else if (routeName === 'Chat') {
          iconName = `ios-chatboxes${focused ? '' : '-outline'}`;
        }

        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },*/
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#f4511e',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  }
);
